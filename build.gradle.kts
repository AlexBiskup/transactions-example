plugins {
    kotlin("jvm") version "1.5.10"
}

group = "at.mindcloud.transactions"
version = "0.0.1"

repositories {
    mavenCentral()
    maven {
        name = "GitLab"
        url = uri("https://gitlab.com/api/v4/projects/25105506/packages/maven")
    }
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("at.mindcloud.transactions:transactions:0.0.1-experimental")
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.0")
}

tasks.withType<Test> {
    useJUnitPlatform()
}
