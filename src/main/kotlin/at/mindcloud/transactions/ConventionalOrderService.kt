package at.mindcloud.transactions

class ConventionalOrderService(private val inventory: ProductInventoryService) : OrderService {

    override fun processOrder(order: Order) {
        val reservationIds = mutableSetOf<Long>()
        var cancellationException: Exception? = null
        for (orderItem in order.orderItems) {
            try {
                val id = this.inventory.reserveProduct(orderItem.productId, orderItem.amount)
                reservationIds.add(id)
            } catch (exception: Exception) {
                for (reservationId in reservationIds) {
                    try {
                        this.inventory.cancelReservation(reservationId)
                    } catch (exception: Exception) {
                        cancellationException = RuntimeException("Failed to cancel reservation $reservationId")
                    }
                }
                break
            }
        }
        if (cancellationException != null) throw cancellationException
    }

}
