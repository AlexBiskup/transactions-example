package at.mindcloud.transactions

class ProductInventoryService(val state: MutableMap<Long, Long>) {

    private var id = 0L

    private val reservations = mutableMapOf<Long, Reservation>()

    var failCancellationOn = emptyList<Long>()

    fun reserveProduct(productId: Long, amount: Long): Long {
        println("Reserve product $productId ${amount}x")
        if (state[productId]?.minus(amount) ?: 0L >= 0L) {
            state[productId] = state[productId]!! - amount
            reservations[id] = Reservation(productId, amount)
        } else {
            throw IllegalStateException("Product $productId out of stock")
        }
        println("Reserved with id $id")
        return id++
    }

    fun cancelReservation(reservationId: Long) {
        println("Cancel reservation with id $reservationId")
        if (reservationId in failCancellationOn) throw IllegalStateException("Cancellation failed")
        val reservation = reservations[reservationId] ?: throw IllegalArgumentException("Reservation $reservationId does not exist")
        reservations.remove(reservationId)
        state[reservation.productId] = state[reservation.productId]!! + reservation.amount
    }


    private data class Reservation(val productId: Long, val amount: Long)

}
