package at.mindcloud.transactions

interface OrderService {

    fun processOrder(order: Order)

}

data class Order(val orderItems: List<OrderItem>) {

    constructor(vararg orderItems: OrderItem) : this(orderItems.toList())

}

data class OrderItem(val productId: Long, val amount: Long)
