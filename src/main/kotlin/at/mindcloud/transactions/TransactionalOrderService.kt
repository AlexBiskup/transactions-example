package at.mindcloud.transactions

class TransactionalOrderService(private val inventory: ProductInventoryService) : OrderService {

    override fun processOrder(order: Order) = transactional<Unit> {
        for (orderItem in order.orderItems) {
            action(
                execute = { inventory.reserveProduct(orderItem.productId, orderItem.amount) },
                revert = { inventory.cancelReservation(reservationId = this.result) },
            )
        }
    }

}
