package at.mindcloud.transactions

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class TransactionsExampleTests {

    private val inventory = ProductInventoryService(
        state = mutableMapOf(
            1L to 10L,
            2L to 5L,
            3L to 8L,
        )
    )

    private val service: OrderService = TransactionalOrderService(inventory)

    @Test
    fun createOrder_withAvailableProducts_reservesProducts() {
        val order = Order(
            OrderItem(productId = 1, amount = 3),
            OrderItem(productId = 2, amount = 2),
        )
        service.processOrder(order)
        val expected = mutableMapOf(
            1L to 7L,
            2L to 3L,
            3L to 8L,
        )
        assertEquals(expected, inventory.state)
    }

    @Test
    fun createOrder_withUnavailableProducts_throwsException() {
        val order = Order(
            OrderItem(productId = 1, amount = 3),
            OrderItem(productId = 2, amount = 2),
            OrderItem(productId = 3, amount = 9L),
        )
        assertThrows<IllegalStateException> {
            service.processOrder(order)
        }
    }

    @Test
    fun createOrder_withUnavailableProducts_cancelsPreviousReservations() {
        val order = Order(
            OrderItem(productId = 1, amount = 3),
            OrderItem(productId = 2, amount = 2),
            OrderItem(productId = 3, amount = 9L),
        )
        try {
            service.processOrder(order)
        } catch (exception: IllegalStateException) {
            println(exception)
        }
        val expected = mutableMapOf(
            1L to 10L,
            2L to 5L,
            3L to 8L,
        )
        assertEquals(expected, inventory.state)
    }

    @Test
    fun createOrder_withErrorOnCancellation_throwsRollbackFailedException() {
        inventory.failCancellationOn = listOf(0L, 1L)
        val order = Order(
            OrderItem(productId = 1, amount = 3),
            OrderItem(productId = 2, amount = 2),
            OrderItem(productId = 3, amount = 9L),
        )
        try {
            service.processOrder(order)
        } catch (exception: RollbackFailedException) {
            exception.printStackTrace()
        }
    }

}
