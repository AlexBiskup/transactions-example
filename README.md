# Transactions Example

Example project for https://gitlab.com/AlexBiskup/transactions.

## Introduction

This projects consists of a
simple [ProductInventoryService](https://gitlab.com/AlexBiskup/transactions-example/-/blob/master/src/main/kotlin/at/mindcloud/transactions/ProductInventoryService.kt)
, which allows reserving products and keeps track of the stock, and
the [OrderService](https://gitlab.com/AlexBiskup/transactions-example/-/blob/master/src/main/kotlin/at/mindcloud/transactions/OrderService.kt)
which should reserve products for orders - but only if all products are in stock.

### OrderService implementation

An implementation of the OrderService could look something
like [this](https://gitlab.com/AlexBiskup/transactions-example/-/blob/master/src/main/kotlin/at/mindcloud/transactions/ConventionalOrderService.kt):

````kotlin
class ConventionalOrderService(private val inventory: ProductInventoryService) : OrderService {

    override fun processOrder(order: Order) {
        val reservationIds = mutableSetOf<Long>()
        var cancellationException: Exception? = null
        for (orderItem in order.orderItems) {
            try {
                val id = this.inventory.reserveProduct(orderItem.productId, orderItem.amount)
                reservationIds.add(id)
            } catch (exception: Exception) {
                for (reservationId in reservationIds) {
                    try {
                        this.inventory.cancelReservation(reservationId)
                    } catch (exception: Exception) {
                        cancellationException = RuntimeException("Failed to cancel reservation $reservationId")
                    }
                }
                break
            }
        }
        if (cancellationException != null) throw cancellationException
    }

}
````

### Transactions OrderService implementation

The following snippet is
an [implementation](https://gitlab.com/AlexBiskup/transactions-example/-/blob/master/src/main/kotlin/at/mindcloud/transactions/TransactionalOrderService.kt)
with the use of the [Transactions](https://gitlab.com/AlexBiskup/transactions) library:

```kotlin
class TransactionalOrderService(private val inventory: ProductInventoryService) : OrderService {

    override fun processOrder(order: Order) = transactional<Unit> {
        for (orderItem in order.orderItems) {
            action(
                execute = { inventory.reserveProduct(orderItem.productId, orderItem.amount) },
                revert = { inventory.cancelReservation(reservationId = this.result) },
            )
        }
    }

}
```

## Unit tests

To understand the expected behaviour in detail please refer to
the [TransactionsExampleTests](https://gitlab.com/AlexBiskup/transactions-example/-/blob/master/src/test/kotlin/at/mindcloud/transactions/TransactionsExampleTests.kt)
.
